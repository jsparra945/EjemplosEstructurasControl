package EjemplosTaller;
import java.util.Scanner;

public class EstructuraIfElse {

	public static Scanner leer;

	public static void main(String[] args) {
		
		leer = new Scanner(System.in);
		
		//Ejemplo estructura de control if else.
		
		int number = 0;
		System.out.println(" \n Ingrese un numero");
        number = leer.nextInt();

		if (number > 5) {

			System.out.println (" \n El numero ingresado es mayor a 5 ");	

		}else if (number < 5){
			
			System.out.println (" \n El numero ingresado es menor a 5 ");
			
		}else {
			
			System.out.println (" \n El numero ingresado es igual a 5 ");
			
		}

	}

}
