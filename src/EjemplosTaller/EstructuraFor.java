package EjemplosTaller;
import java.util.Scanner;

public class EstructuraFor {
	
	public static Scanner leer;

	public static void main(String[] args) {
		
		leer = new Scanner(System.in);
		
		//Ejemplo estructura de control for
		
		System.out.println(" Numeros del 1 al 100");
		
		for(int i = 1; i <= 100; i++) {
			System.out.println(i);
		}
		
	}

}
