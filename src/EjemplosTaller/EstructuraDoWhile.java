package EjemplosTaller;
import java.util.Scanner;

public class EstructuraDoWhile {
	
	public static Scanner leer;

	public static void main(String[] args) {
		
		leer = new Scanner(System.in);
		
		//Ejemplo estructura de control do while
		
		int nota = 0;
		int contadorMayor = 0;
		int contadorMenor = 0;
		
		do {
			
			System.out.println("Por favor la nota de un alumno");
			nota = leer.nextInt();
			if(nota >= 7) {
				contadorMayor = contadorMayor + 1;
			}
			else if(nota>0){
				contadorMenor = contadorMenor + 1;
			}

		    
		} while (nota != 0);

			System.out.println("La cantidad de alumnos que tienen notas mayores o iguales a 7 son : " + contadorMayor);
			System.out.println("La cantidad de alumnos que tienen notas  menores  a 7 son: " + contadorMenor);

	}

}
