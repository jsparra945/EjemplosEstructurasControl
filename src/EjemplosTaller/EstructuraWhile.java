package EjemplosTaller;
import java.util.Scanner;

public class EstructuraWhile {

	public static Scanner leer;
	
	public static void main(String[] args) {
		
		leer = new Scanner(System.in);
		
		//Ejemplo estructura de control while

	
		int numero = -1;

		while (numero <= 0) {
		        System.out.println("\n Ingrese un numero positivo: ");
		        numero = leer.nextInt();
		}
		
		System.out.println("\n El numero ingresado fue: " + numero);

	}

}
