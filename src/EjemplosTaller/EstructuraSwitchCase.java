package EjemplosTaller;
import java.util.Scanner;

public class EstructuraSwitchCase {
	
	public static Scanner leer;

	public static void main(String[] args) {
		
		leer = new Scanner(System.in);
		
		//Ejemplo estructura de control switch case
		
		int number;
		System.out.println("Por favor ingrese un numero del 1 al 5");
		number = leer.nextInt();
		
		switch (number) {
		case 1:
			System.out.print("Perro amigo de juan");
			break;
			
		case 2:
			System.out.print("Conejo amiga de lorena");
			break;
		
		case 3:
			System.out.print("Diego amigo del dinosaurio");
			break;
			
		case 4:
			System.out.print("Nubia amiga de la serpiente");
			break;
			
		case 5:
			System.out.print("Tu amigo del mundo");
			break;

		default:
			System.out.print("La opcion ingresada es incorrecta");
			break;
		}
		
		

	}

}
